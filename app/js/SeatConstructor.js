export class Seat{
	constructor( id, SeatNum, RowNum, type, price ){
		this.id = id;
		this.SeatNum = SeatNum;
		this.RowNum = RowNum;
		this.type = type;		
		this.price = price;
		this.status = 'available';		
	}
}

export default Seat;