export const ModalAddRowRender = () => {
	
	const modalContent = document.getElementsByClassName("modal-content")[0];
	const wrap = document.createElement('div');	
	modalContent.innerHTML = "";

	wrap.innerHTML =
	`
		<span class="close">&times;</span>
		<p>
			<label for="seats">Мест в ряду:</label><br />
			<input type="number" name="seats" id="NumOfSeats" min="1" max="15">
		</p>
		<p>
			<label for="type">Тип ряда:</label><br />
			<select id="selectType">
				<option value="normal">Обычный</option>
				<option value="econom">Эконом</option>
				<option value="premium">Премиум</option>
			</select>
		</p>
		<p><button id="modalBtn">Save</button></p>
	`
	modalContent.appendChild(wrap);
}

export default ModalAddRowRender;