export class Observable {
	constructor(){
		this.observers = [];
		this.sendMessage = this.sendMessage.bind(this);
		this.addObserver = this.addObserver.bind(this);
	}

	sendMessage( e, num, row, price, type, id ){
		this.observers.map( obs => {
			obs.notify( e, num, row, price, type, id );
		});
	}

	addObserver( observer ){
		this.observers.push( observer );
	}
}

export class Observer {
	constructor( action ){
		this.notify = this.notify.bind(this);
		this.action = action;
	}

	notify( e, num, row, price, type, id ){
		this.action( e, num, row, price, type, id );
	}
}

export default Observer;