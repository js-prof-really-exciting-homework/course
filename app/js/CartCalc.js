let message;
let sum;
let targetBlock = document.getElementById('order__sum');

export let cartCalc = () => {	

	if (localStorage.getItem('order')){
		let order = JSON.parse( localStorage.getItem('order') );	
		
		if (order.length !== 0){
			sum = order.reduce( (prev, curr) => {
				return prev+= Number(curr.price);
			},0);

			message = `<b>Сумма: ${sum} грн.</b>`
			targetBlock.innerHTML = message;			
		} else if (order.length === 0) {		
		message = `<b>Сумма: 0 грн.</b>`
		targetBlock.innerHTML = message;
	}

	} else {		
		message = `<b>Сумма: 0 грн.</b>`
		targetBlock.innerHTML = message;

	}
	

}

export {sum};
export default cartCalc;