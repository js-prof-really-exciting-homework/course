import { Observable, Observer } from './Observer';
import {orderArr} from './Cinema';
import cartCalc from './CartCalc';
import timer from './Timer';
import SidebarRender from './Sidebar';

export const obsrvbl = new Observable;

//Добавляем заказ в массив и в Локал Сторедж
const orderToLS = new Observer( (e, num, row, price, type, id) => {
	const sideMenu = document.getElementById('sideMenu');
	const contentBox = sideMenu.querySelector('.sideMenu__content');	
	let order = {
		'id': id,
		'num': num,
		'row': row,
		'price': price		
	}
	if ( localStorage.getItem('order') ){
		const orderLS = JSON.parse( localStorage.getItem('order') );
		let check = orderLS.some( item => {
			return item.id === order.id
		});
		if (check === true){
			return
		} else {
			orderArr.push(order);
		}		
	} else {
		orderArr.push(order);
	}		
	
	localStorage.setItem('order', JSON.stringify(orderArr));
	SidebarRender(); //Заполянем сайдбар
	cartCalc(); // пересчитываем сумму заказа
});

//Генерация бокового меню
const observerSideMenu = new Observer( (e, num, row, price, type) => {
	const sideMenu = document.getElementById('sideMenu');
	const contentBox = sideMenu.querySelector('.sideMenu__content');
	sideMenu.classList.add('sideMenu__show');		
});

//Добавляет класс, который показывает, что место забронировано
const obsAddClass = new Observer ( e => {
	e.target.classList.add('booked');	
});

const obsTimer = new Observer ( timer ); //Включаем таймер

//Меняем статус с available на booked
const obsChangeStatus = new Observer ( (e, num, row, price, type, id) => {
	const rowsLS = JSON.parse( localStorage.getItem('rows') );
	rowsLS.map( row => {		
		for (let seat in row.seats){			
			if (row.seats[seat].id === id){
				row.seats[seat].status = 'booked';				
			}
		}
	});

	localStorage.setItem('rows', JSON.stringify(rowsLS) )
});

obsrvbl.addObserver(observerSideMenu);
obsrvbl.addObserver(orderToLS);
obsrvbl.addObserver(obsAddClass);
obsrvbl.addObserver(obsTimer);
obsrvbl.addObserver(obsChangeStatus);

export default obsrvbl;