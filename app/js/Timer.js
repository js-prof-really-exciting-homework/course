import {orderArr} from './Cinema';

const timer = () => {
	let orderTimer = document.getElementById('order__timer');
	let count = 60;
	const rowsLS = JSON.parse( localStorage.getItem('rows') );

	//Очищаем таймер, если он есть
	if(orderTimer.timer) {
		clearInterval(orderTimer.timer);
	}

	//Ставим 60 сек
	orderTimer.innerHTML =
		`
			${count}
		`
	orderTimer.style.color = ''

	//Запускаем таймер
	orderTimer.timer = setInterval( () => {
		count--
		orderTimer.innerHTML =
		`
		${count}
		`
		if(count === 10){ //Make it orange!
			orderTimer.style.color = 'orange';
		} else if(count === 5){ //Make it red!
			orderTimer.style.color = 'red';
		} else if (count === 0){
			clearInterval(orderTimer.timer);
			orderTimer.innerHTML =
			`
			<img src="./img/boom.png" width="200px">
			`
			localStorage.removeItem('order'); //Удаляем заказ из локал сторедж
			orderArr.splice( 0, orderArr.length );
			let cart = document.querySelector('.sideMenu__content');
			let sum = document.getElementById('order__sum');
			let seats = document.querySelectorAll('button');
			let seatsArr = Array.from(seats)

				cart.innerHTML = ""; // Очищаем корзину
				sum.innerHTML = "";
				
				seatsArr.map( item =>{
					item.classList.remove('booked'); //Удаляем класс booked, чтобы места были доступны
					item.dataset.status = 'available';
				});

				//Меняем статус мест на available
				rowsLS.map( row => {		
					for (let seat in row.seats){
						row.seats[seat].status = 'available';
					}
				});

	localStorage.setItem('rows', JSON.stringify(rowsLS) )		
		}
	},1000);



}

export default timer;