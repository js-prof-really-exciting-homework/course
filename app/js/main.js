import Cinema from './Cinema';

const BuildCinema = () => {
	document.addEventListener('DOMContentLoaded', () => {
		let newCinema = new Cinema();
		newCinema.render();
		console.log(newCinema);
	});	
}


export default BuildCinema;