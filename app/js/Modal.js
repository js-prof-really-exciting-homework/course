import ModalAddRowRender from './ModalAddRowRender';

const Modal = (e) => {
	const modal = document.getElementById('myModal');
	const modalContent = document.getElementsByClassName("modal-content")[0];

	if (e.target.dataset.type = "add"){
		ModalAddRowRender();		
	}
	//Show modal
	modal.style.display = "block";
	// Get the <span> element that closes the modal
	const span = modalContent.querySelector(".close");		
	// When the user clicks on <span> (x), close the modal
	span.addEventListener('click', () => {
		modal.style.display = "none";
	});

	// When the user clicks anywhere outside of the modal, close it
	window.addEventListener('click', e => {
		  if (e.target === modal) {
	    modal.style.display = "none";
	  }
	});
}

export default Modal;