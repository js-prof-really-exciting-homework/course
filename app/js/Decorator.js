import {NormalRow, EconomRow, PremiumRow} from './RowClasses';

class Decorator{
	constructor( seats, type, number ){

		if ( type === 'normal' ){
			return new NormalRow( seats, type, number );
		} else if( type === 'econom' ){
			return new EconomRow( seats, type, number );
		} else if( type === 'premium' ){
			return new PremiumRow( seats, type, number );
		}
	}
}

export default Decorator;