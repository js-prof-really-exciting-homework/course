'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('autoprefixer');

sass.compiler = require('node-sass');

gulp.task('styles', function(){
	return gulp.src('./app/styles/main.scss')
			.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
			.pipe(postcss([ autoprefixer() ]))
			.pipe(gulp.dest('./public/css'));
});

gulp.task('watch:styles', function(){
	gulp.watch('./app/styles/*.scss', gulp.series('styles'))
});

